const { writeFileSync, readFileSync } = require("fs");
let version = process.env.CI_COMMIT_TAG || "0.1.0";

version = version.replace(/^v/i, "");

let pacakge = JSON.parse(readFileSync('./package.json', { encoding: "utf-8" }));
pacakge.version = version;

writeFileSync("package.json", JSON.stringify(pacakge, null, 2));

# PHP Extension Superpack!

Included extensions:
- [`felixfbecker.php-debug`](https://marketplace.visualstudio.com/items?itemName=felixfbecker.php-debug):<br /> 
  For PHP Debugging with [XDebug](https://xdebug.org/).
- [`neilbrayfield.php-docblocker`](https://marketplace.visualstudio.com/items?itemName=neilbrayfield.php-docblocker):<br />
  To help you documentate those functions and classes!
- [`bmewburn.vscode-intelephense-client`](https://marketplace.visualstudio.com/items?itemName=bmewburn.vscode-intelephense-client):<br />
  Help you see class' functions and properties with its documentations with the help of IntelliSense.
- [`ikappas.phpcs`](https://marketplace.visualstudio.com/items?itemName=ikappas.phpcs):<br />
  Standarize your (and your teammate's) code with the help of [PHP
  CodeSniffer](https://github.com/squizlabs/PHP_CodeSniffer). Additional setup may be required.
- [`onecentlin.phpunit-snippets`](https://marketplace.visualstudio.com/items?itemName=onecentlin.phpunit-snippets):<br />
  Make unit test faster with the help of PHPUnit snippets and macros.
- [`wongjn.php-sniffer`](https://marketplace.visualstudio.com/items?itemName=wongjn.php-sniffer):<br />
  Auto-format your code with CodeSniffer's rule set that you've set up. Additional setup may be needed.

## Install

To install this you just need to copy this line and then run it on the Quick Open menu (<kbd>Ctrl</kbd> + <kbd>P</kbd>):
```
ext install chez14.php-superpack
```
## Additional Setup

### phpcs

You need to install `squizlabs/php_codesniffer` Composer Package to make this extension work as expected. There's two way on how to install this.

1. System-wide:
    use Composer:
    ```sh
    $ composer global require squizlabs/php_codesniffer
    ```

2. Project-wide:
   ```sh
   $ composer require squizlabs/php_codesniffer
   ```

For more information please see:
https://marketplace.visualstudio.com/items?itemName=ikappas.phpcs#linter-installation.

### PHP Sniffer (wongjn)

This extension requires you to set up your vscode formatter settings. You can do that in two ways:

1. System-wide
    - Open the Settings page (<kbd>Ctrl</kbd> + <kbd title="comma">,</kbd>). 
    - Make sure you're in User tab. 
    - Search `defaultFormatter`. 
    - Change to `wongjn.php-sniffer`.
    - You can also edit the `settings.json` file for user-wide settings.

2. Project-wide
    1. via Settings page (<kbd>Ctrl</kbd> + <kbd title="comma">,</kbd>).
         - Open the Settings page, make sure you're in the Workspace Tab.
         - Search `defaultFormatter`. 
         - Change to `wongjn.php-sniffer`.
    2. via `settings.json` file
         - Create a file on `.vscode/settings.json`.
         - Add following snippets to set the default formatter to this extensions:
        ```json
        {
            "[php]": {
                "editor.defaultFormatter": "wongjn.php-sniffer"
            }
        }
        ```

... and then to setup your extension settings, you need to add the following piece on the `settings.json`, either on your User or Workspace.

```json
{
  "phpSniffer.autoDetect": true
}
```

For more information please see:
https://marketplace.visualstudio.com/items?itemName=wongjn.php-sniffer#extension-settings

## Suggestions and Contacts
Please send suggestions on the repo: https://gitlab.com/chez14/php-superpack.